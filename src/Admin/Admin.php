<?php
namespace App\Admin;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Admin Extends DB{

    private $id,$name, $editid, $userName, $firstName, $lastName, $email, $password, $confirmPass, $deptName, $authorname, $subject_name, $bookName, $bpub, $b_aut, $purdate, $price, $bremark, $b_rack, $subjectid, $activate, $date, $news,$redirect,$table;

    private $studentid,$bookid,$issuedate,$returndate,$remarks;


    public function setData($postData)    {
        if (array_key_exists('studentid', $postData)) {
            $this->studentid = $postData['studentid'];
        }
        if (array_key_exists('bookid', $postData)) {
            $this->bookid = $postData['bookid'];
        }
        if (array_key_exists('issuedate', $postData)) {
            $this->issuedate = $postData['issuedate'];
        }
        if (array_key_exists('returndate', $postData)) {
            $this->returndate = $postData['returndate'];
        }
        if (array_key_exists('remarks', $postData)) {
            $this->remarks = $postData['remarks'];
        }
        if (array_key_exists('userName', $postData)) {
            $this->userName = $postData['userName'];
        }
        if (array_key_exists('studentName', $postData)) {
            $this->studentName = $postData['studentName'];
          
        }
        if (array_key_exists('name', $postData)) {
              $this->studentName = $postData['name'];
        }
        if (array_key_exists('dob', $postData)) {
              $this->date = $postData['dob'];
        }
        if (array_key_exists('firstName', $postData)) {
            $this->firstName = $postData['firstName'];
        }
        if (array_key_exists('lastName', $postData)) {
            $this->lastName = $postData['lastName'];
        }
        if (array_key_exists('email', $postData)) {
            $this->email = $postData['email'];
        }
        if (array_key_exists('password', $postData)) {
            $this->password = $postData['password'];
        }
        if (array_key_exists('confirmPass', $postData)) {
            $this->confirmPass = $postData['confirmPass'];
        }
        if (array_key_exists('deptname', $postData)) {
            $this->deptName = $postData['deptname'];
        }
        if (array_key_exists('authorname', $postData)) {
            $this->authorname = $postData['authorname'];
        }
        if (array_key_exists('subject_name', $postData)) {
            $this->subject_name = $postData['subject_name'];
        }
        if (array_key_exists('bookName', $postData)) {
            $this->bookName = $postData['bookName'];
        }
        if (array_key_exists('bpub', $postData)) {
            $this->bpub = $postData['bpub'];
        }
        if (array_key_exists('b_aut', $postData)) {
            $this->b_aut = $postData['b_aut'];
        }
        if (array_key_exists('purdate', $postData)) {
            $this->purdate = $postData['purdate'];
        }
        if (array_key_exists('price', $postData)) {
            $this->price = $postData['price'];
        }
        if (array_key_exists('bremark', $postData)) {
            $this->bremark = $postData['bremark'];
        }
        if (array_key_exists('b_rack', $postData)) {
            $this->b_rack = $postData['b_rack'];
        }
        if (array_key_exists('subjectid', $postData)) {
            $this->subjectid = $postData['subjectid'];
        }
        if (array_key_exists('activate', $postData)) {
            $this->activate = $postData['activate'];
        }

        if (array_key_exists('date', $postData)) {
            $this->date = $postData['date'];
        }
        if (array_key_exists('news', $postData)) {
            $this->news = $postData['news'];
        }
        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }
        if (array_key_exists('editid', $postData)) {
            $this->editid = $postData['editid'];
            $this->id = $postData['editid'];
        }
    }

    public function store()
    {


        $arrData = '';
        $sql = '';

           if (isset($_POST['issuebook'])) {
            $arrData = array($this->studentid, $this->bookid, $this->issuedate,$this->returndate,$this->remarks);
            $sql = "Insert Into issuebook(student_id, Book_id, Issue_date, Return_date,) values(?,?,?,?)";
             $this->redirect = 'add.php?id=issuebook';
        }
        if (isset($_POST['addstaff'])) {
            $arrData = array($this->userName, $this->firstName, $this->lastName, $this->email, $this->password);
            $sql = "Insert Into user(first_name, last_name, username, email, password) values(?,?,?,?,?)";
            $this->redirect = 'view.php?id=staff';
        }
        if (isset($_POST['addstudent'])) {
            $arrData = array($this->studentName, $this->date);
                        //var_dump($arrData); die();
            $sql = "Insert Into student(name,dob) values(?,?)";
            $this->redirect = 'view.php?id=student';
        }

        if (isset($_POST['adddept'])) {
            $arrData = array($this->deptName);
            $sql = "Insert Into department(deptname) values(?)";
            $this->redirect = 'view.php?id=department';
        }
        if (isset($_POST['author'])) {
            // var_dump($_POST); die();
            $arrData = array($this->authorname);
            $sql = "Insert Into author (authorname) values(?)";
            $this->redirect = 'view.php?id=author';
        }
        if (isset($_POST['addsubject'])) {
            $arrData = array($this->subject_name);
            $sql = "Insert Into subject (subject_name) values(?)";
            $this->redirect = 'view.php?id=subject';
           // var_dump($this->subject_name); die();            
        }
        if (isset($_POST['addbook'])) {
            $arrData = array($this->bookName, $this->bpub, $this->b_aut, $this->purdate, $this->price, $this->activate);


            $sql = "Insert Into book (B_Title,B_Pub,authorid,B_PurDate,B_Price,Activate ) values(?,?,?,?,?,?)";
            $this->redirect = 'view.php?id=book';
        }
        if (isset($_POST['addnews'])) {
            $arrData = array($this->date, $this->news);
            $sql = "Insert Into news (date,description) values(?,?)";
            $this->redirect = 'view.php?id=news';
        }


        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if ($result)
            Message::message("Succes! Data has been Inserted :)");
        else
            Message::message(" Failed! Data has not been inserted");

        Utility::redirect("$this->redirect");


    }

    public function view(){
        //var_dump($_GET); die();
        $sql = '';
        $STH = '';

        if ($_GET['id'] == 'staff') { $sql = "select * from  user  WHERE soft_delete='NO' AND type <> 'Admin' AND type <> 'Staff'"; }
        if ($_GET['id'] == 'student') { $sql = "select * from  student WHERE soft_delete='No'";  }
        if ($_GET['id'] == 'subject') {   $sql = "select * from  subject WHERE soft_delete='No'";   }
        if ($_GET['id'] == 'news') { $sql = "select * from  news WHERE soft_delete='No'";   }
        if ($_GET['id'] == 'department') { $sql = "select * from  department WHERE soft_delete='No'";}
        if ($_GET['id'] == 'book') { $sql = "select * from  book WHERE soft_delete='No' ";}
        if ($_GET['id'] == 'author') {$sql = "select * from  author WHERE soft_delete='No'"; }
        if ($_GET['id'] == 'allstudent') {$sql = "select * from  student WHERE soft_delete='No'"; }
        if ($_GET['id'] == 'allbook') {$sql = "select * from  book WHERE soft_delete='No' AND available='Yes'"; }
        if ($_GET['id'] == 'issuebook') {$sql = "select issuebook.id, issuebook.Issue_date,issuebook.Return_date, student.id as sid,book.id as bid, student.name, book.B_Title from  issuebook RIGHT join student ON issuebook.student_id=student.id RIGHT join book ON issuebook.Book_id=book.id WHERE issuebook.soft_delete='No' AND available='Yes'"; }
         $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();
        Utility::redirect($_SERVER['HTTP_REFERER']);
    }


public function trashView(){
        //var_dump($_GET); die();
        $sql = '';
        if ($_GET['id'] == 'staff') { $sql = "select * from  user  WHERE soft_delete='Yes'"; }
        if ($_GET['id'] == 'student') { $sql = "select * from  student WHERE soft_delete='Yes'"; }
        if ($_GET['id'] == 'subject') {  $sql = "select * from  subject WHERE soft_delete='Yes'";}
        if ($_GET['id'] == 'author') {$sql = "select * from  author WHERE soft_delete='Yes'";}
        if ($_GET['id'] == 'news') {$sql = "select * from  news WHERE soft_delete='Yes'";}
        if ($_GET['id'] == 'department'){$sql = "select * from  department WHERE soft_delete='Yes'";}
        if ($_GET['id'] == 'book') {$sql = "select * from  book WHERE soft_delete='Yes'";}
        if ($_GET['id'] == 'issuebook') {$sql = "select issuebook.id, issuebook.Issue_date,issuebook.Return_date, student.id as sid,book.id as bid, student.name, book.B_Title from  issuebook RIGHT join student ON issuebook.student_id=student.id RIGHT join book ON issuebook.Book_id=book.id WHERE issuebook.soft_delete='Yes'";}
         $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();

    }

    public function editView(){
       
       $sql='';
        if ($_GET['updateid']=='updatestaff'){$sql = "select * from  user WHERE id='$this->editid'";  }
        if($_GET['updateid']=='updatestudent'){$sql = "select * from  student WHERE id='$this->editid'";}
        if($_GET['updateid']=='updatedepartment'){$sql = "select * from department WHERE id='$this->editid'";}
        if($_GET['updateid']=='updateauthor'){$sql = "select * from author WHERE id='$this->editid'";}
        if($_GET['updateid']=='updatesubject'){$sql = "select * from subject WHERE id='$this->editid'";}
        if($_GET['updateid']=='updatenews'){$sql = "select * from news WHERE id='$this->editid'";}
        if($_GET['updateid']=='updatebook'){$sql = "select * from book WHERE id='$this->editid'";}
        if($_GET['updateid']=='updateissuebook'){$sql = "select issuebook.id,issuebook.student_id, issuebook.Issue_date,issuebook.Return_date, student.name, book.id as bid, book.B_Title from  issuebook RIGHT join student ON issuebook.student_id=student.id RIGHT join book ON issuebook.Book_id=book.id WHERE issuebook.soft_delete='No' AND available='Yes' AND issuebook.id=$this->id";}
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
        Utility::redirect($_SERVER['HTTP_REFERER']);
    }
    public function deleteView(){
//var_dump($_GET);die();
        $sql='';
        if ($_GET['finaldeleteid']=='deletestaff'){$sql = "select * from  user WHERE id='$this->id'";  }
        if($_GET['finaldeleteid']=='deletestudent'){$sql = "select * from  student WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deletedepartment'){$sql = "select * from department WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deleteauthor'){
          //  var_dump($sql);die();
            $sql = "select * from author WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deletesubject'){$sql = "select * from subject WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deletenews'){
           // var_dump($sql);die();
            $sql = "select * from news WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deletebook'){$sql = "select * from book WHERE id='$this->id'";}
        if($_GET['finaldeleteid']=='deleteissuebook'){$sql = "select issuebook.id,issuebook.student_id, issuebook.Issue_date,issuebook.Return_date,issuebook.remarks, student.name, book.id as bid, book.B_Title from  issuebook RIGHT join student ON issuebook.student_id=student.id RIGHT join book ON issuebook.Book_id=book.id WHERE issuebook.soft_delete='Yes' AND available='Yes' AND issuebook.id=$this->id";}
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
       //return $STH->fetchAll();
        return $STH->fetch();
    }
    public function update(){
       // var_dump($_POST); die();
        $arrData = '';
        $sql = '';
        
        if ($_POST['type']=='updateStaff'){
            $arrData = array( $this->firstName, $this->lastName,$this->userName,$this->password, $this->email);
            //var_dump($arrData); die();

            $sql = "UPDATE user SET first_name=?, last_name=?,username=?,password=?,email=? WHERE id='$this->id'";
            $this->redirect = 'view.php?id=staff';
        }

        if ($_POST['type']=='updatestudent'){

            $arrData = array( $this->studentName, $this->date);
           // var_dump($arrData); die();
            $sql = "UPDATE student SET  name=?, dob=? WHERE id='$this->id'";
             $this->redirect = 'view.php?id=student';
        }
        if ($_POST['type']=='updatedepartment'){
//var_dump($_POST); die();
            $arrData = array( $this->deptName);
           // var_dump($arrData); die();
            $sql = "UPDATE department SET  deptname=? WHERE id='$this->id'";
             $this->redirect = 'view.php?id=department';
        }
        if ($_POST['type']=='updateauthor'){

            $arrData = array( $this->authorname);
           // var_dump($arrData); die();
            $sql = "UPDATE author SET  authorname=? WHERE id='$this->id'";
             $this->redirect = 'view.php?id=author';
        }
        if ($_POST['type']=='updatesubject'){

            
               //var_dump($this->subject_name); die();
            $arrData = array( $this->subject_name);

            $sql = "UPDATE subject SET  subject_name=? WHERE id='$this->id'";
            $this->redirect = 'view.php?id=subject';
        }
        if ($_POST['type']=='updatenews'){

            $arrData = array( $this->date,$this->news);
            // var_dump($arrData); die();
            $sql = "UPDATE news SET  date=?,description=? WHERE id='$this->id'";
            $this->redirect = 'view.php?id=news';
        }
        if ($_POST['type']=='updatebook'){

            $arrData = array( $this->bookName, $this->bpub, $this->b_aut, $this->purdate, $this->price, $this->bremark, $this->b_rack, $this->subjectid, $this->activate);
            // var_dump($arrData); die();
            $sql = "UPDATE book SET B_Title=?,B_Pub=?,authorid=?,B_PurDate=?,B_Price=?,B_Remark=?,B_Rack_No=?,B_Subject_Id=?,Activate=? WHERE id='$this->id'";
            $this->redirect = 'view.php?id=book';
        }
         if ($_POST['type']=='updateissuebook'){

            $arrData = array( $this->studentid, $this->bookid, $this->issuedate, $this->returndate, $this->remarks);
            // var_dump($arrData); die();
            $sql = "UPDATE issuebook SET student_id=?,Book_id=?,Issue_date=?,Return_date=?,remarks=? WHERE id='$this->id'";
            $this->redirect = 'view.php?id=issuebook';
        }
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if ($result) Message::message("Succes! Data has been Updated:)");
        else {
            Message::message(" Failed! Data has not been Updated");
        }
       Utility::redirect("$this->redirect");

    }
    public function trash(){
       // var_dump($_GET); die();
       $sql = "UPDATE user SET soft_delete='Yes' WHERE id='$this->id'";
     
        $sql = '';

        if ($_GET['deleteid']=='deletestaff'){
           // var_dump($_GET);die();
            $sql = "UPDATE user SET soft_delete='Yes' WHERE id='$this->id'";
        }

        if ($_GET['deleteid']=='deletestudent'){
            $sql = "UPDATE student SET soft_delete='Yes' WHERE id=$this->id";
        }

        if ($_GET['deleteid']=='deletedepartment'){
          //  var_dump($_GET); die();
            $sql = "UPDATE department SET soft_delete='Yes' WHERE id=$this->id";
        }

        if ($_GET['deleteid']=='deleteauthor'){
            $sql = "UPDATE author SET soft_delete='Yes' WHERE id=$this->id";
        }

        if ($_GET['deleteid']=='deletesubject'){
            $sql = "UPDATE subject SET soft_delete='Yes' WHERE id=$this->id";
        }

        if ($_GET['deleteid']=='deletebook'){
            $sql = "UPDATE book SET soft_delete='Yes' WHERE id=$this->id";
        }

        if ($_GET['deleteid']=='deleteissuebook'){
            $sql = "UPDATE issuebook SET soft_delete='Yes' WHERE id=$this->id";
        }


        if ($_GET['deleteid']=='deletenews'){
            $sql = "UPDATE news SET soft_delete='Yes' WHERE id=$this->id";
        }

        $result =$this->DBH->exec($sql);

        if ($result) Message::message("Succes! Data has been soft deleted :)");
        else {
            Message::message(" Failed! Data has not been deleted");
        }
        Utility::redirect($_SERVER['HTTP_REFERER']);

    }
     public function recover(){
      // var_dump($_GET); die();
       
     
        $sql = '';

        if ($_GET['recoverid']=='recoverstaff'){
           // var_dump($_GET);die();
            $sql = "UPDATE user SET soft_delete='No' WHERE id='$this->id'";
        }

        if ($_GET['recoverid']=='recoverstudent'){
            $sql = "UPDATE student SET soft_delete='No' WHERE id=$this->id";
        }

        if ($_GET['recoverid']=='recoverdepartment'){
          //  var_dump($_GET); die();
            $sql = "UPDATE department SET soft_delete='No' WHERE id=$this->id";
        }

        if ($_GET['recoverid']=='recoverauthor'){
            $sql = "UPDATE author SET soft_delete='No' WHERE id=$this->id";
        }

        if ($_GET['recoverid']=='recoversubject'){
            $sql = "UPDATE subject SET soft_delete='No' WHERE id=$this->id";
        }

        if ($_GET['recoverid']=='recoverbook'){
            $sql = "UPDATE book SET soft_delete='No' WHERE id=$this->id";
        }

        if ($_GET['recoverid']=='recoverissuebook'){
            $sql = "UPDATE issuebook SET soft_delete='No' WHERE id=$this->id";
        }


        if ($_GET['recoverid']=='recovernews'){
            $sql = "UPDATE news SET soft_delete='No' WHERE id=$this->id";
        }

        $result =$this->DBH->exec($sql);

        if ($result) Message::message("Succes! Data has been recovered :)");
        else {
            Message::message(" Failed! Data has not been recovered");
        }
        Utility::redirect($_SERVER['HTTP_REFERER']);

    }
    public function delete(){
       //var_dump($_GET); die();
     
        $sql = '';
        $page='';
        if ($_GET['finaldeleteid']=='deletestaff'){
           // var_dump($_GET);die();
            $sql = "Delete from  user WHERE id='$this->id'";$page='staff';
        }

        if ($_GET['finaldeleteid']=='deletestudent'){
            $sql = "Delete from  student WHERE id='$this->id'";$page='student';
        }

        if ($_GET['finaldeleteid']=='deletedepartment'){
          //  var_dump($_GET); die();
            $sql = "Delete from department WHERE id='$this->id'"; $page='department';
        }

        if ($_GET['finaldeleteid']=='deleteauthor'){
            $sql = "Delete from  author WHERE id='$this->id'"; $page='author';
        }

        if ($_GET['finaldeleteid']=='deletesubject'){
            $sql = "Delete from subject WHERE id='$this->id'"; $page='subject';
        }

        if ($_GET['finaldeleteid']=='deletebook'){
        $sql = "Delete from book WHERE id='$this->id'"; $page='book';
        }

        if ($_GET['finaldeleteid']=='deleteissuebook'){
            $sql = "Delete from issuebook WHERE id='$this->id'"; $page='issuebook';
        }


        if ($_GET['finaldeleteid']=='deletenews'){
            $sql = "Delete from news WHERE id='$this->id'"; $page='news';
        }

        $result =$this->DBH->exec($sql);

        if ($result) Message::message("Succes! Data has been  deleted :)");
        else {
            Message::message(" Failed! Data has not been deleted");
        }
        Utility::redirect('trashed.php?id='.$page);

    }
    public function trashMultiple(){
      //  var_dump($_POST); die();
        $selectedIDsArray=$_POST['mark'];
        if($_POST['deleteid']=='staff'){ $this->table='user'; }
        if($_POST['deleteid']=='book'){ $this->table='book'; }
        if($_POST['deleteid']=='author'){ $this->table='author'; }
        if($_POST['deleteid']=='department'){ $this->table='department'; }
        if($_POST['deleteid']=='issuebook'){ $this->table='issuebook'; }
        foreach($selectedIDsArray as $id){
            $sql = "UPDATE  $this->table SET soft_delete='Yes' WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");

        Utility::redirect($_SERVER['HTTP_REFERER']);

    }
    public function recoverMultiple(){
       //var_dump($_POST); die();
        $selectedIDsArray=$_POST['mark'];
        if($_POST['recoverid']=='staff'){ $this->table='user'; }
        if($_POST['recoverid']=='student'){ $this->table='student'; }
        if($_POST['recoverid']=='book'){ $this->table='book'; }
        if($_POST['recoverid']=='author'){ $this->table='author'; }
        if($_POST['recoverid']=='department'){ $this->table='author'; }
        if($_POST['recoverid']=='issuebook'){ $this->table='issuebook'; }
        foreach($selectedIDsArray as $id){
            $sql = "UPDATE  $this->table SET soft_delete='No' WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");

        Utility::redirect($_SERVER['HTTP_REFERER']);

    }
    public function deleteMultiple(){
        //  var_dump($_POST); die();
        $selectedIDsArray=$_POST['mark'];
        if($_POST['recoverid']=='staff'){ $this->table='user'; }
        if($_POST['recoverid']=='book'){ $this->table='book'; }
        if($_POST['recoverid']=='author'){ $this->table='author'; }
        if($_POST['recoverid']=='department'){ $this->table='author'; }
        if($_POST['recoverid']=='issuebook'){ $this->table='issuebook'; }
        foreach($selectedIDsArray as $id){
            $sql = "DELETE from  $this->table WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");

        Utility::redirect($_SERVER['HTTP_REFERER']);

    }
}