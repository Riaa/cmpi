<?php
require_once('../../vendor/autoload.php');
include('../namespace.php');
include('header.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>CMPI  online Library System</title>

    <script>
        function getEdata()
        {

            var tr=document.getElementById("soption").value;
            var str=document.getElementById("Boption").value;
            if(tr=="All")
                str="open";
            if (str=="" || tr=="")
            {

                document.getElementById("tnt1").innerHTML="Please enter search keyword";
                document.getElementById("tnt").innerHTML="";
                return;
            }
            if (window.XMLHttpRequest)
            {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }
            else
            {// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    document.getElementById("tnt1").innerHTML="";
                    document.getElementById("tnt").innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET","getpdf.php?q="+str+"&r="+tr,true);
            xmlhttp.send();
        }
    </script>



    <style type="text/css">
        .style1
        {
            font-size: 36px
        }
    </style>
</head>
<body>

<div class="container_12">
    <br/>

            <h2 text align="center" > Download E-notes </h2>
    <br>
    <br>

            <div class="module-body"><!--put tab here -->


                <form name="edit_book" action="" method="post">


                    <table width="100%">
                        <tr>
                            <td width="185" height="45" align="right">Search Option : </td>
                            <td width="207" align="left">
                                <select name="soption" class="subject input-long" id="soption">
                                    <option value="All">View All</option>
                                    <option value="sub">Subject name</option>
                                    <option value="tit">Title</option>
                                </select>
                            </td>
                            <td width="498">Enter Keyword  :
                                <input type="text" name="Boption" id="Boption" class="input-medium" /> &nbsp; &nbsp;
                                <input type="button" class="submit-green"  onclick="getEdata()" height="50" width="150" value="Search" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div> <!-- End .module-body -->
        </div>  <!-- End .module -->
        <div style="clear:both;"></div>

        <!--else -->
        <div id="tnt1" align="center" style="color:#990000;"></div>
        <div id="tnt"> </div>



    </div> <!-- End .grid_12 -->

    <div style="clear:both;"></div>
</div> <!-- End .container_12 -->



</body>
</html>
<br>
<br>

<?php
include('footer.php');
include('footer_script.php');
?>