<?php
require_once('../vendor/autoload.php');
//include('namespace.php');
include('header.php');

?>
    <div class="container">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 main">
                <div id="menu" align="center">
                    <div   style="position:center; width:560px; right: 500px; top: 170px; height: 415px; border:solid;  border-color: #c5b9f0; padding:15px;  background-color:#c5b9f0">
                        <h3><label>Write Us:</label></h3>
                        <form>
                            <div class="form-group">
                                <label for="fullname">Full Name:</label>
                                <input type="text" class="form-control" placeholder="FULL NAME" id="fullname" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" placeholder="EMAIL" required>
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment:</label>
                                <textarea class="form-control" rows="5" id="5" placeholder="Enter your message…" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>

<?php
include ('footer.php');
include ('footer_script.php');
?>