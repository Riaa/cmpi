<?php
require_once('../vendor/autoload.php');
//include('namespace.php');
include('header.php');

?>


<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 main">
            <?php
            $conn =new mysqli('localhost', 'root', '' , 'hmpilibrary');
            $sql = $conn->prepare("SELECT * FROM book");
            $sql->execute();
            $result = $sql->get_result();
            if ($result->num_rows > 0) {
                echo "<table  border='solid 1px' width='100%'> 
            <tr ><th>ID</th><th>B_Title</th><th>B_Pub</th><th>B_Aut</th><th>B_PurDate</th><th>B_Price</th><th>B_Remark</th><th>Activate</th></tr>";
                while ($row = mysqli_fetch_assoc($result)) {

                    echo "<tr><td>".$row['Bid']." </td> <td>".$row['B_Title']."</td><td>".$row['B_Pub']."</td><td>".$row['B_Aut']."</td><td>".$row['B_PurDate']."</td><td>".$row['B_Price']."</td><td>".$row['B_Remark']."</td><td>".$row['B_Subject_Id']."</td><td></tr>";
                }
                echo "</table>";
            }
            $conn->close();

            ?>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>

    <br>
<?php
include_once ('footer.php');
include_once ('footer_script.php');
?>