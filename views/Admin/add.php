<?php
require_once('../../vendor/autoload.php');
include('../namespace.php');
include('header.php');


 $objCmpi = new \App\Admin\Admin();
    $objCmpi->setData($_POST);
   ?>

<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6 main">
           <?php
           if($_GET['id']=='staff') {  ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Create User</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>User Name</td>
                           <td>:</td>
                           <td><input type="hidden"  name="addstaff" value="addstaff">
                               <input type="text" class="form-control" name="userName" required>
                               <input type="hidden"  name="type" value="Staff">
                           </td>
                       </tr>
                       <tr>
                           <td>First Name</td>
                           <td>:</td>
                           <td><input type="text" class="form-control" name="firstName" required></td>
                       </tr>
                       <tr>
                           <td>LastName</td>
                           <td>:</td>
                           <td><input type="text" class="form-control" name="lastName" required></td>
                       </tr>
                       <tr>
                           <td>Email</td>
                           <td>:</td>
                           <td><input type="email" class="form-control" name="email" required></td>
                       </tr>

                       <tr>
                           <td>Password</td>
                           <td>:</td>
                           <td><input type="password" class="form-control" name="password" required></td>
                       </tr>
                       <tr>
                           <td>Confirm Password</td>
                           <td>:</td>
                           <td><input type="password" class="form-control" name="confirmPass" required></td>
                       </tr>
                       <tr>
                           <td></td>
                           <td></td>
                           <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
           <?php  }
           if($_GET['id']=='student') {
           ?>
               <form action="store.php" method="post" name="student" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Create User</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Student Name</td>
                           <td>:</td>
                           <td><input type="hidden"  name="addstudent" value="addstudent">
                               <input type="text" class="form-control" name="studentName" required>
                               <input type="hidden"  name="type" value="Staff">
                           </td>
                       </tr>

                       <tr>
                           <td>Birth Date</td>
                           <td>:</td>
                           <td><input type="date" class="form-control" name="dob" required></td>
                       </tr>

                       <tr>
                           <td></td>
                           <td></td>
                           <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
        <?php  }
           if($_GET['id']=='dept') {
           ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Add Department</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Department Name</td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="adddept" value="adddept">
                               <input type="text" class="form-control" name="deptname" required>

                           </td>
                       </tr>




                       <td></td>
                       <td></td>
                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>

           <?php  }
           if($_GET['id']=='author') {
           ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Add Author</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Athore Name</td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="author" value="author">
                               <input type="text" class="form-control" name="authorname" required>

                           </td>
                       </tr>

                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
           <?php  }
           if($_GET['id']=='subject') {
           ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Add Subject</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Subject Name</td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="addsubject" value="addsubject">
                               <input type="text" class="form-control" name="subject_name" required>

                           </td>
                       </tr>

                       <td></td>
                       <td></td>
                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
          
  <?php  }
           if($_GET['id']=='book') {
             
           ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Add Book</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Title</td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="addbook" value="addbook">
                               <input type="text" class="form-control" name="bookName">

                           </td>
                       </tr>
                       <tr>
                           <td>Publish</td>
                           <td>:</td>
                           <td>

                               <input type="date" class="form-control" name="bpub" >

                           </td>
                       </tr>  <tr>
                           <td>Author</td>
                           <td>:</td>
                           <td>

                               <input type="text" class="form-control" name="b_aut" >

                           </td>
                       </tr>  <tr>
                           <td>PurDate</td>
                           <td>:</td>
                           <td>

                               <input type="date" class="form-control" name="purdate" >

                           </td>
                       </tr>  <tr>
                           <td>Price</td>
                           <td>:</td>
                           <td>

                               <input type="number" class="form-control" name="price" >

                           </td>
                       </tr>                    
                           </td>
                       </tr> 
                         
                       <tr>
                           <td>Activate</td>
                           <td>:</td>
                           <td>

                               <input type="number" class="form-control" name="activate" required>

                           </td>
                       </tr>
                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
               <?php  }
           if($_GET['id']=='issuebook') {
            $_GET['id']='allstudent';
            $objCmpi->setData($_GET);
           $allstudent= $objCmpi-> view();

           $_GET['id']='allbook';
            $objCmpi->setData($_GET);
           $allbook= $objCmpi-> view();
           ?>

               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Issue Book </p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Student </td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="issuebook" value="issuebook">
                               <select name="studentid"  id="studentid" class="form-control text-uppercase">                               
                                <?php 
                                foreach ($allstudent as $oneData) {
                                  echo "<option  value='$oneData->id'>$oneData->name</option>";
                                }
                                ?>
                              </select> 
                           </td>
                       </tr>
                       <tr>
                           <td>Book </td>
                           <td>:</td>
                           <td>
                               <select id="bookid" name="bookid" class="form-control text-uppercase">                               
                                <?php 
                                foreach ($allbook as $oneData) {
                                  echo "<option  value='$oneData->id'>$oneData->B_Title</option>";
                                }
                                ?>
                              </select> 
                           </td>
                       </tr>  <tr>
                           <td>Issue Date</td>
                           <td>:</td>
                           <td>

                               <input type="date" class="form-control" name="issuedate" >

                           </td>
                       </tr>  <tr>
                           <td>Return Date</td>
                           <td>:</td>
                           <td>

                               <input type="date" class="form-control" name="returndate" >

                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
           <?php  }
           if($_GET['id']=='news') {
           ?>
               <form action="store.php" method="post" class="signleTranscation">
                   <div class="control">
                       <div class="row">
                           <div class="col-md-6">
                               <a href="#" class="btn btn-secondary">EDIT</a>
                               <a href="#" class="btn btn-secondary">Refresh</a>
                           </div>
                           <div class="col-md-6">
                               <p class="nick text-right">Add News</p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Date</td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="addnews" value="addnews">
                               <input type="date" class="form-control" name="date" required>

                           </td>
                       </tr>

                       <tr>
                           <td>Description</td>
                           <td>:</td>
                           <td>

                               <input type="text" class="form-control" name="news" required>

                           </td>
                       </tr>




                       <td></td>
                       <td></td>
                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>
               </form>
        <?php }

           ?>

        </div>
        <div class="col-sm-3"></div>
    </div>
</div>
 <?php
include('footer.php');
include('footer_script.php');
?>