<?php
require_once('../../vendor/autoload.php');
include('namespace.php');
include('header.php');
use App\Message\Message;
use App\Utility\Utility;

if(isset($_POST['mark'])) {

    $objMultiple=new \App\Admin\Admin();

    $objMultiple->trashMultiple($_POST);

}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect($_SERVER['HTTP_REFERER']);
}