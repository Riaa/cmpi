<?php
require_once('../../vendor/autoload.php');
include('namespace.php');
include('header.php');
use App\Message\Message;
use App\Utility\Utility;
//var_dump($_POST); die();

if(isset($_POST['mark'])) {

    $objMultiple=new \App\Admin\Admin();

    $objMultiple->recoverMultiple($_POST);

}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect($_SERVER['HTTP_REFERER']);
}