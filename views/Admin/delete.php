<?php
require_once('../../vendor/autoload.php');
include('namespace.php');
include('header.php');

//var_dump($_POST); die();

$objStaff = new \App\Admin\Admin();
$objStaff->setData($_GET);

if(isset($_POST['mark'])){

	$objStaff->setData($_POST);
	$objStaff->deleteMultiple();
}

if(isset($_GET['YesButton']))
{

	$objStaff->setData($_GET);
	$objStaff->delete();

}
else{

if (isset($_GET['finaldeleteid'])){

$oneData=$objStaff->deleteView();
	//var_dump($oneData);
//$objStaff->delete();
$yesButton=$_GET['finaldeleteid'];
?>
	<?php
	################################### delete staff #########################
	if($_GET['finaldeleteid']=='deletestaff') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th>Name</th>
							<th>Email/Username</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->first_name</td>
                     <td>$oneData->email </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete student #########################
	if($_GET['finaldeleteid']=='deletestudent') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th>Student Name</th>
							<th>Birth Date</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->dob </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete department #########################
	if($_GET['finaldeleteid']=='deletedepartment') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
						<th>Department</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->deptname </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete author #########################
	if($_GET['finaldeleteid']=='deleteauthor') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>

							<th>AthorName</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->authorname </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete subject #########################
	if($_GET['finaldeleteid']=='deletesubject') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th>subject_name</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                       <td>$oneData->subject_name </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete news #########################
	if($_GET['finaldeleteid']=='deletenews') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th>Date</th>
							<th>Description</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->date</td>
                     <td>$oneData->description </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete book #########################
	if($_GET['finaldeleteid']=='deletebook') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th>Book Name</th>
							<th>Price</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->B_Title</td>
                    <td>$oneData->B_Price </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}
	################################### delete issuebook  #########################
	if($_GET['finaldeleteid']=='deleteissuebook') {


		?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>
					<table class="table table-striped table-bordered border" cellspacing="0px">
						<tr>
							<th style='width: 10%; text-align: center'>ID</th>
							<th> Book Name</th>
							<th> Student Name</th>
						</tr>
						<?php
						echo "
		       <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->B_Title </td>
                  </tr>
              ";
						?>
					</table>

					<?php

					echo "
          <a href='delete.php?id=$oneData->id&YesButton=1&finaldeleteid=$yesButton' class='btn btn-danger'>Yes</a>
          <a href=" . $_SERVER['HTTP_REFERER'] . " class='btn btn-success'>No</a>
        ";
					?>
				</div>
				<div class="col-md-1"></div>
				<br>
			</div>
		</div>
		<?php
	}

}




else if (isset($_GET['recoverid'])){
	$viewData=$objStaff->recover();
}
else{
	$viewData=$objStaff->trash();

}

}
//$objToArray = json_decode(json_encode($viewData), True);

//var_dump($_GET);dirname(path)e();



include('footer.php');
include('footer_script.php');
?>
