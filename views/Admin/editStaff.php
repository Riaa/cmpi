<?php
require_once('../../vendor/autoload.php');
include('../namespace.php');
include('header.php');

$objStaff = new \App\Admin\Admin();
$objCmpi = new \App\Admin\Admin();
$objStaff->setData($_GET);
$viewData=$objStaff->editView();
$objToArray = json_decode(json_encode($viewData), True);

//var_dump($viewData);die();
?>

<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6 main">
            <form action="update.php" method="post" class="signleTranscation">
                <div class="control">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-secondary">EDIT</a>
                            <a href="#" class="btn btn-secondary">Refresh</a>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <?php 
                if($_GET['updateid']=='updatestaff'){ ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>User Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="userName"  value="<?php echo $objToArray[0]['username']; ?>"required>
                                <input type="hidden"  name="type" value="updateStaff">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>First Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="firstName" value="<?php echo $objToArray[0]['first_name'];; ?>" required></td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="lastName" value="<?php echo $objToArray[0]['last_name']; ?>" required></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><input type="email" class="form-control" name="email" value="<?php echo $objToArray[0]['email']; ?>" required></td>
                        </tr>


                        <tr>
                            <td>Password</td>
                            <td>:</td>
                            <td><input type="password" class="form-control" name="password" required></td>
                        </tr>
                        <tr>
                            <td>Confirm Password</td>
                            <td>:</td>
                            <td><input type="password" class="form-control" name="confirmPass" required></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>
                        </tr>
                    </table>

                <?php }
                if($_GET['updateid']=='updatestudent'){ ?>
                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>user Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="name"  value="<?php echo $objToArray[0]['name']; ?>"required>
                                <input type="hidden"  name="type" value="updatestudent">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>BIrth of Date</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="dob" value="<?php echo $objToArray[0]['dob'];; ?>" required></td>
                        </tr>

                        <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>

                <?php }
                if($_GET['updateid']=='updatedepartment'){ 
            
                    ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>Department</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="deptname"  value="<?php echo $objToArray[0]['deptname']; ?>"required>
                                <input type="hidden"  name="type" value="updatedepartment">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>


                        <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>

                <?php }
                if($_GET['updateid']=='updateauthor'){ ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>Author Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="authorname"  value="<?php echo $objToArray[0]['authorname']; ?>"required>
                                <input type="hidden"  name="type" value="updateauthor">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>
                        <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>

                <?php }
                if($_GET['updateid']=='updatesubject'){ ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>Subject Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="subject_name"  value="<?php echo $objToArray[0]['subject_name']; ?>"required>
                                <input type="hidden"  name="type" value="updatesubject">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>


                        <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>

                <?php }
                if($_GET['updateid']=='updatenews'){ ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>Date</td>
                            <td>:</td>
                            <td><input type="date" class="form-control" name="=date"  value="<?php echo $objToArray[0]['news']; ?>"required>
                                <input type="hidden"  name="type" value="updatesubject">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>News</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="news" value="<?php echo $objToArray[0]['description']; ?>" required></td>
                        </tr>
                        <tr>

                        <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>
                <?php }
                if($_GET['updateid']=='updatebook'){ ?>

                    <table class="table table-responsive" border="0">

                        <tr>
                            <td>Book Name</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="=bookname"  value="<?php echo $objToArray[0]['B_Title']; ?>"required>
                                <input type="hidden"  name="type" value="updatebook">
                                <input type="hidden"  name="id" value="<?php echo $objToArray[0]['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Publish</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="b_pub" value="<?php echo $objToArray[0]['B_Pub']; ?>" required></td>
                        </tr>
                         <tr>
                            <td>Author</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="b_pub" value="<?php echo $objToArray[0]['B_aut']; ?>" required></td>
                        </tr>
                         <tr>
                            <td>PurDate</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="b_pub" value="<?php echo $objToArray[0]['B_Pub']; ?>" required></td>
                        </tr>
                         <tr>
                            <td>Price</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="b_pub" value="<?php echo $objToArray[0]['B_Pub']; ?>" required></td>
                        </tr>
                    
                      
                        <tr>

                            <td><input type="submit" class="btn btn-primary" name="submit" value="Save"  href=editStaff.php></td>

                    </table>

                    <?php }
                if($_GET['updateid']=='updateissuebook'){ 
//var_dump($_GET);
var_dump($objToArray);

            $_GET['id']='allstudent';
            $objCmpi->setData($_GET);
           $allstudent= $objCmpi-> view();
//var_dump( $allstudent);
           $_GET['id']='allbook';
            $objCmpi->setData($_GET);
           $allbook= $objCmpi-> view();


           ?>

               
                   <div class="control">
                       <div class="row">
                          
                           <div class="col-md-12">
                               <p class="nick text-right">Issue Book </p>
                           </div>
                       </div>
                   </div>
                   <table class="table table-responsive" border="0">

                       <tr>
                           <td>Student </td>
                           <td>:</td>
                           <td>
                               <input type="hidden"  name="issuebook" value="issuebook">
                               <input type="hidden"  name="type" value="updateissuebook">
                               <input type="hidden"  name="editid" value="<?php echo $objToArray[0]['id']; ?>">
                               <select name="studentid"  id="studentid" class="form-control text-uppercase">                               
                                <?php 
                                foreach ($allstudent as $oneData) {

                                  if($oneData->id==$objToArray[0]['student_id']){  $select="selected";} else{ $select=" "; }

                                  echo "<option ".$select." value='$oneData->id'>$oneData->name</option>";
                                  
                                    
                                }
                                ?>
                              </select> 
                           </td>

                       </tr>
                       <tr>
                           <td>Book </td>
                           <td>:</td>
                           <td>
                           <?php //echo "<pre>"; var_dump($allbook); echo "</pre>"; ?>
                               <select id="bookid" name="bookid" class="form-control text-uppercase">                               
                                <?php 

                                foreach ($allbook as $oneData) {
                                   if($oneData->id==$objToArray[0]['bid']){  $select="selected";} else{ $select=" "; }
                                  echo "<option ".$select."  value='$oneData->id'>$oneData->B_Title</option>";
                                }
                                ?>
                              </select> 
                           </td>
                       </tr>  <tr>
                           <td>Issue Date</td>
                           <td>:</td>
                           <td>
                               <input type="date" class="form-control" name="issuedate"  value="<?php echo $objToArray[0]['Issue_date']; ?>" >


                           </td>
                       </tr>  <tr>
                           <td>Return Date</td>
                           <td>:</td>
                           <td>

                               <input type="date" class="form-control" name="returndate" value="<?php echo $objToArray[0]['Return_date']; ?>">
                         <tr>
                           <td>Remarks</td>
                           <td>:</td>
                           <td>

                               <textarea class="form-control" name="remarks"   > <?php echo $objToArray[0]['remarks']; ?></textarea>

                           </td>
                       </tr>
                       <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                       </tr>
                   </table>

                <?php } ?>
               

            </form>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>

 <?php 
include('footer.php');
include('footer_script.php');
?> 
 