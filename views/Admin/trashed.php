<?php
require_once('../../vendor/autoload.php');
include('../namespace.php');
include('header.php');

//var_dump($_GET); die();

//if($_GET['id']=='staff') {

$objStaff = new \App\Admin\Admin();
$objStaff->setData($_GET);
$viewData=$objStaff->trashView();
//var_dump($viewData); die();
//}

?>
 
<div class="container">
    <form name="multiple" id="multiple" action="recovermultiple.php" method="post">

 <div class="row">
        <div class="col-sm-1"></div>
        <div class=" text-right col-sm-10 ">
            <button type="submit" class="btn btn-primary">Download As PDF</button>
            <button type="submit" class="btn btn-primary">Download As  </button>
            <button type="submit" id="delete" class="btn btn-danger">Delete Selected</button>
            <button type="submit" class="btn btn-success">Recover Selected</button>
            <input type="hidden" name="recoverid" value="<?php echo ($_GET['id']); ?>">

            <a class="btn btn-info" href="view.php?id=<?php echo $_GET['id']; ?>"> Active List </a>
        </div>
        <div class="col-sm-1"></div>
  </div>      
     <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 main">

          <?php
          $selectAllTh='<th ><div align=\'center\'>Select All<br> <input id="select_all" type="checkbox" value="select all"></div></th>';
          $selectAllTr="<td ><div align='center'><input type='checkbox' class='checkbox' name='mark[]' value=";

            ################################### view staff #########################
            if($_GET['id']=='staff') {
                echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Trashed Staff List </h4>
            <tr > $selectAllTh <th>ID</th><th>Name</th><th>Email</th><th>Action</th></tr>";
                foreach ($viewData as $oneData) {
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td> <td> $oneData->id </td> <td> $oneData->first_name $oneData->last_name</td> <td>$oneData->email </td><td> <a href='delete.php?id=$oneData->id&recoverid=recoverstaff'  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletestaff class='btn btn-danger'>Delete</a></td></tr>";
                }
                echo "</table>";
            }
            ################################### view student #########################
            if($_GET['id']=='student') {
                echo "<table  border='solid 1px' width='100%'>
   <h4 style=\"text-align: center\" ;\"> Trashed Student List </h4>
            <tr > $selectAllTh<th>ID</th><th>Name</th><th>Birth Date</th><th>Action</th></tr>";
                foreach ($viewData as $oneData) {
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td> <td> $oneData->id </td> <td> $oneData->name </td> <td>$oneData->dob</td><td> <a href='delete.php?id=$oneData->id&recoverid=recoverstudent'  class=\"btn btn-success\"=>Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletestudent class='btn btn-danger'>Delete</a></td></tr>";
                }
                echo "</table>";
            }
          ################################### view department #########################
          if($_GET['id']=='department') {
              echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\"> Trashed Department List </h4>
             <tr >$selectAllTh<th>ID</th><th>Department</th><th>Action</th></tr>";
              foreach ($viewData as $oneData){
                  echo "<tr>$selectAllTr '$oneData->id'> </div></td> <td> $oneData->id</td> <td> $oneData->deptname</td> <td> <a href=delete.php?id=$oneData->id&recoverid=recoverdepartment  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletedepartment class='btn btn-danger'>Delete</a></td></tr>";
              }
              echo "</table>";
          }

          ################################### view author #########################
          if($_GET['id']=='author') {
              echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\"> Trashed Author List </h4>
             <tr >$selectAllTh<th>ID</th><th>AuthorName</th><th>Action</th></tr>";
              foreach ($viewData as $oneData){
                  echo "<tr>$selectAllTr '$oneData->id'> </div></td><td> $oneData->id</td> <td> $oneData->authorname</td> <td><a href=delete.php?id=$oneData->id&recoverid=recoverauthor class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deleteauthor class='btn btn-danger'>Delete</a></td></tr>";
              }
              echo "</table>";
          }
            ################################### view subject #########################
            if($_GET['id']=='subject') {
                echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\"> Trashed Subject List </h4>
            <tr >$selectAllTh<th>ID</th><th>Subject Name</th><th>Action</th></tr>";
                foreach ($viewData as $oneData){
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td><td> $oneData->id</td> <td> $oneData->subject_name <td> <a href=delete.php?id=$oneData->id&recoverid=recoversubject  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletesubject class='btn btn-danger'>Delete</a></td></tr>";
                }
                echo "</table>";
            }
            ################################### view news #########################
            if($_GET['id']=='news') {
                echo "<table  border='solid 1px' width='100%'> 
             <tr >$selectAllTh<th>ID</th><th>Date</th><th>Description</th><th>Action</th></tr>";
                foreach ($viewData as $oneData){
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td><td> $oneData->id</td> <td> $oneData->date </td><td>$oneData->description </td><td> <a href=delete.php?id=$oneData->id&recoverid=recovernews  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletenews class='btn btn-danger'>Delete</a></td></tr>";
                }
                echo "</table>";
            }

            ################################### view book #########################
            if($_GET['id']=='book') {
                echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\"> Trashed Book List </h4>
             <tr >$selectAllTh<th>ID</th><th>Book Name</th><th>Book Publish</th><th>Author</th><th>PurDate</th><th>Price</th><th>Action</th></tr>";
                foreach ($viewData as $oneData){
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td><td> $oneData->id</td> <td> $oneData->B_Title</td> <td> $oneData->B_Pub</td> <td> $oneData->authorid</td> <td> $oneData->B_PurDate</td> <td> $oneData->B_Price</td> <td> <a href=delete.php?id=$oneData->id&recoverid=recoverbook  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deletebook class='btn btn-danger'>Delete</a></td></tr>";
                }        
                echo "</table>";
              } 

              ################################### view issuebook #########################
            if($_GET['id']=='issuebook') {
                echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\"> Trashed IssueBook List </h4>
             <tr >$selectAllTh<th>ID</th><th>Student Name</th><th>Book Name</th><th>Issue Date</th><th>Return Date</th><th>Price</th></tr>";
                foreach ($viewData as $oneData){
                    echo "<tr>$selectAllTr '$oneData->id'> </div></td><td> $oneData->id</td> <td> $oneData->name</td> <td> $oneData->B_Title</td> <td> $oneData->Issue_date</td> <td> $oneData->Return_date</td>  <td> <a href=delete.php?id=$oneData->id&recoverid=recoverissuebook  class=\"btn btn-success\">Recover</a> <a  href=delete.php?id=$oneData->id&finaldeleteid=deleteissuebook class='btn btn-danger'>Delete</a></td></tr>";
                }        
                echo "</table>";
              }     
            ?>
                
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
</form>

 <?php 
include('footer.php');
include('footer_script.php');
?>
