
<?php
require_once('../../vendor/autoload.php');
include('namespace.php');
include('header.php');

//var_dump($_GET); die();

//if($_GET['id']=='staff') {

$objStaff = new \App\Admin\Admin();
$objStaff->setData($_GET);
$viewData=$objStaff->view();
//var_dump($viewData); die();
//}
//var_dump($_GET);

$html="";
$trs="";
$sl=0;
$thead="";
?>


<style>
    table th  {
        text-align: center;
    }
</style>

<div class="container">

        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 main">

                <?php


                ################################### view staff #########################

                if($_GET['id']=='staff') {
                    $thead= "<table  border='solid 1px' width='100%'>  <h4 style=\"text-align: center\" ;\">  Active Staff List </h4>
            <tr >  <th>ID</th><th>Name</th><th>Email</th></tr>";


                    foreach ($viewData as $oneData) {
                        $id=$oneData->id;
                        $firstName=$oneData->first_name;
                        $lastName=$oneData->last_name;
                        $email=$oneData->email;


                        $trs.= "<tr>";
                        $trs.="<td> $id </td>";
                        $trs.="<td> $firstName $lastName </td>";
                        $trs.="<td> $email </td>";
                        $trs.= "</tr>";
                    }

                }
                ################################### view student #########################
                if($_GET['id']=='student') {
                    echo "<table  border='solid 1px' width='100%'>
   <h4 style=\"text-align: center\" ;\">  Active Student List </h4>
            <tr > $selectAllTh<th>ID</th><th>Name</th><th>Birth Date</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData) {
                        echo "<tr> $selectAllTr '$oneData->id'></div></td><td> $oneData->id </td> <td> $oneData->name </td> <td>$oneData->dob</td><td> <a href=editStaff.php?editid=$oneData->id&updateid=updatestudent class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deletestudent class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }
                ################################### view department #########################
                if($_GET['id']=='department') {
                    echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Active depertment List </h4>
             <tr > $selectAllTh<th>ID</th><th>Department</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr>$selectAllTr '$oneData->id'></div></td><td> $oneData->id</td> <td> $oneData->deptname</td> <td> <a href=editStaff.php?editid=$oneData->id&updateid=updatedepartment  class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deletedepartment class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }

                ################################### view author #########################
                if($_GET['id']=='author') {
                    echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Active Author List </h4>
             <tr > $selectAllTh<th>ID</th><th>AuthorName</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr> $selectAllTr '$oneData->id'></div></td></td><td> $oneData->id</td> <td> $oneData->authorname</td> <td><a href=editStaff.php?editid=$oneData->id&updateid=updateauthor  class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deleteauthor class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }
                ################################### view subject #########################
                if($_GET['id']=='subject') {
                    echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Active Subject List </h4>
            <tr > $selectAllTh<th>ID</th><th>Subject Name</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr> $selectAllTr '$oneData->id'></div></td><td> $oneData->id</td> <td> $oneData->subject_name <td> <a href=editStaff.php?editid=$oneData->id&updateid=updatesubject  class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deletesubject class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }
                ################################### view news #########################
                if($_GET['id']=='news') {
                    echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Available  News List </h4>
             <tr > $selectAllTh<th>ID</th><th>Date</th><th>Description</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr>$selectAllTr '$oneData->id'></div></td><td> $oneData->id</td> <td> $oneData->date </td><td>$oneData->description </td><td> <a href=editStaff.php?editid=$oneData->id&updateid=updatenews class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deletenews class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }

                ################################### view book #########################
                if($_GET['id']=='book') {
                    echo "<table  border='solid 1px' width='100%'>
 <h4 style=\"text-align: center\" ;\">  Book List </h4>
             <tr > $selectAllTh<th>ID</th><th>Book Name</th><th>Book Publish</th><th>Author_ID</th><th>PurDate</th><th>Price</th><th>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr> $selectAllTr '$oneData->id'></div></td><td> $oneData->id</td> <td> $oneData->B_Title</td> <td> $oneData->B_Pub</td> <td> $oneData->authorid</td> <td> $oneData->B_PurDate</td> <td> $oneData->B_Price</td>  <td> <a href=editStaff.php?editid=$oneData->id&updateid=updatebook class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deletebook class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }

                ################################### view issuebook #########################
                if($_GET['id']=='issuebook') {
                    echo "<h4 align='center'>List of issued book</h4>";

                    echo "<table  border='solid 1px' width='100%' style='text-aling:center;'>
             <tr > $selectAllTh<th class='text-center'>ID</th><th class='text-center'>Student Name</th><th class='text-center'> Book Name</th><th class='text-center'>Issue Date</th><th class='text-center'>Return Date</th><th class='text-center'>Action</th></tr>";
                    foreach ($viewData as $oneData){
                        echo "<tr> $selectAllTr '$oneData->id'></div></td><td class='text-center'> $oneData->id</td> <td> $oneData->name</td> <td> $oneData->B_Title</td> <td> $oneData->Issue_date</td> <td> $oneData->Return_date</td> <td> <a href=editStaff.php?editid=$oneData->id&updateid=updateissuebook class=\"btn btn-primary\">Edit</a> <a  href=delete.php?id=$oneData->id&deleteid=deleteissuebook  class=\"btn btn-warning\">Delete</a></td></tr>";
                    }
                    echo "</table>";
                }

                ?>

            </div>
            <div class="col-sm-1"></div>
        </div>
</div>


<?php
$html=<<<EOD
              <table  border='solid 1px' width='100%'>
                  <thead>

                  </thead>
                   <tbody>
                     $trs
                    </tbody>
              </table>

EOD;

require_once ('../../vendor/mpdf/mpdf/mpdf.php');

$FPDF=new mPDF();

$FPDF->WriteHTML($html);


//output a pdf file directory to the browser
$FPDF ->Output('list.pdf','D');



include('footer.php');
include('footer_script.php');
?>
